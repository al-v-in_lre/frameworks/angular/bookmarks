import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Bookmark } from '../models/bookmark';
import { Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { BookmarkState, RequestStatus } from '../state/bookmark.state';
import { selectAllBookmarks, selectBookmarkIsLoading, selectBookmarkError, selectBookmarkInsertStatus, selectBookmarkSelection } from '../state/bookmark.selectors';
import * as bookmarkActions from '../state/bookmark.actions';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-bookmark-list',
  templateUrl: './bookmark-list.component.html',
  styleUrls: ['./bookmark-list.component.scss']
})
export class BookmarkListComponent implements OnInit {
  private subscription: Subscription = new Subscription();
  bookmarks$: Observable<Bookmark[]>;
  bookmarkFormGroup = this.formBuilder.group({
    id: [null],
    label: [''],
    url: ['']
  });

  constructor(private store$: Store<BookmarkState>,
    private formBuilder: FormBuilder) {}

  ngOnInit(): void {
    this.subscription.add(
      this.store$.select(selectBookmarkIsLoading).subscribe( isLoading => {
        console.log('Is loading:', isLoading );
      }));

    this.subscription.add(
      this.store$.select(selectBookmarkError).subscribe( error => {
        console.log('Error:', error );
      }));

    this.subscription.add(
      this.store$.select(selectBookmarkInsertStatus)
        .pipe(
          filter(insertStatus => insertStatus == RequestStatus.SUCCESS)
        )
        .subscribe( insertStatus => {
          console.log('Req status:', RequestStatus[insertStatus]);
        }));

    this.subscription.add(
      this.store$.select(selectBookmarkSelection).subscribe( bookmark => {
        if(bookmark)
          this.bookmarkFormGroup.setValue(bookmark);
        else
          this.bookmarkFormGroup.reset();
      }));

    this.bookmarks$ = this.store$.select(selectAllBookmarks);

    this.store$.dispatch(bookmarkActions.loadRequestAction());
  }

  onSubmit(){
    if (this.bookmarkFormGroup.valid) {
      const bookmark = this.bookmarkFormGroup.value;
      if(bookmark.id)
        this.store$.dispatch(bookmarkActions.updateRequestAction({bookmark: bookmark}));
      else
        this.store$.dispatch(bookmarkActions.insertRequestAction({bookmark: bookmark}));
    }
  }

  onDelete(bookmark: Bookmark): void {
    this.store$.dispatch(bookmarkActions.deleteRequestAction({id: bookmark.id}));
  }

  onEdit(bookmark: Bookmark){
    this.store$.dispatch(bookmarkActions.selectBookmarkAction({bookmark: bookmark}));
  }

  onNew(){
    this.store$.dispatch(bookmarkActions.unselectBookmarkAction());
  }
}
