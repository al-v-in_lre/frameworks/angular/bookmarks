export interface Bookmark{
  id: number,
  label: string,
  url: string
}
