import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { Bookmark } from '../models/bookmark';

export const bookmarkAdapter: EntityAdapter<Bookmark> = createEntityAdapter<Bookmark>({
  selectId: bookmark => bookmark.id,
  sortComparer: (a: Bookmark, b: Bookmark): number => a.id.toString().localeCompare(b.id.toString())
});

export enum RequestStatus{
  IDLE,
  IN_PROGRESS,
  SUCCESS,
  FAILURE
}

export interface BookmarkState extends EntityState<Bookmark>{
  isLoading?: boolean;
  insertStatus?: RequestStatus,
  error?: string;
  selectedBookmark?: Bookmark
}

export const initialState: BookmarkState = bookmarkAdapter.getInitialState({
  isLoading: false,
  insertStatus: RequestStatus.IDLE,
  error: null,
  selectedBookmark: null
});
