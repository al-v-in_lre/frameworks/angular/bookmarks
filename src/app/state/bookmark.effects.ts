import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { BookmarkDataService } from '../data-services/bookmark-data.service';
import { map, mergeMap, catchError, concatMap, withLatestFrom, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';
import * as bookmarkActions from './bookmark.actions';
import { BookmarkState } from './bookmark.state';
import { Store, select, Action } from '@ngrx/store';
import { selectBookmarkSelection } from './bookmark.selectors';

@Injectable()
export class BookmarkEffects{
  loadBookmarks$ = createEffect(
    () => this.actions$.pipe(
      ofType(bookmarkActions.loadRequestAction),
      mergeMap(
        () => this.bookmarkDataService.getBookmarks().pipe(
          map(bookmarks => bookmarkActions.loadSuccessAction({bookmarks: bookmarks})),
          catchError(error => of(bookmarkActions.loadFailureAction({error: error})))
        )
      )
    )
  );

  insertBookmark$ = createEffect(
    () => this.actions$.pipe(
      ofType(bookmarkActions.insertRequestAction),
      mergeMap(
        (action) => this.bookmarkDataService.addBookmark(action.bookmark).pipe(
          switchMap((bookmark) => [
            bookmarkActions.insertSuccessAction({bookmark: bookmark}),
            bookmarkActions.selectBookmarkAction({bookmark: bookmark})
          ]),
          catchError(error => of(bookmarkActions.insertFailureAction({error: error})))
        )
      )
    )
  );

  updateBookmark$ = createEffect(
    () => this.actions$.pipe(
      ofType(bookmarkActions.updateRequestAction),
      mergeMap(
        (action) => this.bookmarkDataService.updateBookmark(action.bookmark).pipe(
          map(() => bookmarkActions.updateSuccessAction({bookmark: action.bookmark})),
          catchError(error => of(bookmarkActions.updateFailureAction({error: error})))
        )
      )
    )
  );

  deleteBookmark$ = createEffect(
    () => this.actions$.pipe(
      ofType(bookmarkActions.deleteRequestAction),
      concatMap(action => of(action).pipe(
        withLatestFrom(this.store$.pipe(select(selectBookmarkSelection)))
      )),
      mergeMap(
        ([action, selectedBookmark]) => this.bookmarkDataService.deleteBookmark(action.id).pipe(
          switchMap(() => {
            const actions: Action[] = [bookmarkActions.deleteSuccessAction({id: action.id})];
            if(selectedBookmark && selectedBookmark.id == action.id){
              actions.push(bookmarkActions.unselectBookmarkAction());
            }
            return actions;
          }),
          catchError(error => of(bookmarkActions.deleteFailureAction({error: error})))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private  bookmarkDataService: BookmarkDataService,
    private store$: Store<BookmarkState>
  ){}
}
