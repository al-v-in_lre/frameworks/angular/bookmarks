import { BookmarkState, bookmarkAdapter, RequestStatus } from './bookmark.state';
import { createFeatureSelector, createSelector, select } from '@ngrx/store';
import { Bookmark } from '../models/bookmark';

export const BOOKMARK_FEATURE_KEY = 'bookmarks';

export const selectBookmarkState = createFeatureSelector<BookmarkState>(BOOKMARK_FEATURE_KEY);

export const selectBookmarkIsLoading = createSelector(
  selectBookmarkState,
  (state: BookmarkState): boolean => state.isLoading
);

export const selectBookmarkError = createSelector(
  selectBookmarkState,
  (state: BookmarkState): string => state.error
);

export const selectBookmarkInsertStatus = createSelector(
  selectBookmarkState,
  (state: BookmarkState): RequestStatus => state.insertStatus
);

export const selectBookmarkSelection = createSelector(
  selectBookmarkState,
  (state: BookmarkState): Bookmark => state.selectedBookmark
);

// export const selectBookmarkAdded = () => {
//   return pipe(
//     select(selectAllBookmarks),
//     pairwise(),
//     map( ([bookmarkIdsPrev, bookmarkIdsCurrent]) => {
//       return _.differenceWith(bookmarkIdsCurrent, bookmarkIdsPrev, (a, b) => a.id === b.id);
//     } )
//   );
// }

const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = bookmarkAdapter.getSelectors(selectBookmarkState);

export const selectBookmarkIds = selectIds;
export const selectBookmarkEntities = selectEntities;
export const selectAllBookmarks = selectAll;
export const selectBookmarkCountTotal = selectTotal;


export const selectBookmarkGetById = id => createSelector(
  selectBookmarkEntities,
  entities => entities[id]
);
