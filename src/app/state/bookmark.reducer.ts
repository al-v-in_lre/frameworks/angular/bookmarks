import { createReducer, on } from '@ngrx/store';
import { bookmarkAdapter, initialState, RequestStatus } from './bookmark.state';
import * as bookmarkActions from './bookmark.actions';

export const bookmarkReducer = createReducer(
  initialState,
  //load
  on(bookmarkActions.loadRequestAction, state => ({...state, isLoading: true, error: null, insertStatus: RequestStatus.IDLE}) ),
  on(bookmarkActions.loadFailureAction, (state, {error}) => ({...state, isLoading: false, error: error}) ),
  on(bookmarkActions.loadSuccessAction, (state, {bookmarks}) => bookmarkAdapter.addAll(bookmarks, {...state, isLoading: false, error: null}) ),
  //insert
  on(bookmarkActions.insertRequestAction, (state) => ({...state, error: null, insertStatus: RequestStatus.IN_PROGRESS}) ),
  on(bookmarkActions.insertFailureAction, (state, {error}) => ({...state, error: error, insertStatus: RequestStatus.FAILURE}) ),
  on(bookmarkActions.insertSuccessAction, (state, {bookmark}) => bookmarkAdapter.upsertOne(bookmark, {...state, error: null, insertStatus: RequestStatus.SUCCESS}) ),
  //update
  on(bookmarkActions.updateRequestAction, (state) => ({...state, isLoading: true, error: null, insertStatus: RequestStatus.IDLE}) ),
  on(bookmarkActions.updateFailureAction, (state, {error}) => ({...state, isLoading: false, error: error}) ),
  on(bookmarkActions.updateSuccessAction, (state, {bookmark}) => bookmarkAdapter.upsertOne(bookmark, {...state, isLoading: false, error: null}) ),
  //delete
  on(bookmarkActions.deleteRequestAction, (state) => ({...state, isLoading: true, error: null, insertStatus: RequestStatus.IDLE}) ),
  on(bookmarkActions.deleteFailureAction, (state, {error}) => ({...state, isLoading: false, error: error}) ),
  on(bookmarkActions.deleteSuccessAction, (state, {id}) => bookmarkAdapter.removeOne(id, {...state, isLoading: false, error: null}) ),
  //selection
  on(bookmarkActions.selectBookmarkAction, (state, {bookmark}) => ({...state, selectedBookmark: bookmark})),
  on(bookmarkActions.unselectBookmarkAction, (state) => ({...state, selectedBookmark: null})),
);
