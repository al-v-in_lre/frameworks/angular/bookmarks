import { createAction, props, union } from '@ngrx/store';
import { Bookmark } from '../models/bookmark';

export const loadRequestAction = createAction('[Bookmarks] Load Request');
export const loadFailureAction = createAction('[Bookmarks] Load Failure', props<{ error: string }>());
export const loadSuccessAction = createAction('[Bookmarks] Load Success', props<{ bookmarks: Bookmark[] }>());

export const insertRequestAction = createAction('[Bookmarks] Insert Request', props<{ bookmark: Bookmark }>());
export const insertFailureAction = createAction('[Bookmarks] Insert Failure', props<{ error: string }>());
export const insertSuccessAction = createAction('[Bookmarks] Insert Success', props<{ bookmark: Bookmark }>());

export const updateRequestAction = createAction('[Bookmarks] Update Request', props<{ bookmark: Bookmark }>());
export const updateFailureAction = createAction('[Bookmarks] Update Failure', props<{ error: string }>());
export const updateSuccessAction = createAction('[Bookmarks] Update Success', props<{ bookmark: Bookmark }>());

export const deleteRequestAction = createAction('[Bookmarks] Delete Request', props<{ id: number }>());
export const deleteFailureAction = createAction('[Bookmarks] Delete Failure', props<{ error: string }>());
export const deleteSuccessAction = createAction('[Bookmarks] Delete Success', props<{ id: number }>());

export const selectBookmarkAction = createAction('[Bookmarks] Select', props<{ bookmark: Bookmark }>());
export const unselectBookmarkAction = createAction('[Bookmarks] Unselect');
