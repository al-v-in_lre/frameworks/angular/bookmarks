import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, timeout } from 'rxjs/operators';

import { Bookmark } from '../models/bookmark';


@Injectable({
  providedIn: 'root'
})
export class BookmarkDataService {
  private bookmarksUrl = 'api/bookmarks';

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(
    private http: HttpClient) { }

  private log(message: string) {
    console.log(`BookmarkDataService: ${message}`);
  }

  getBookmarks(): Observable<Bookmark[]> {
    return this.http.get<Bookmark[]>(this.bookmarksUrl)
      .pipe(
        tap(_ => this.log('fetched bookmarks'))
      );
  }

  updateBookmark(bookmark: Bookmark): Observable<any> {
    return this.http.put(this.bookmarksUrl, bookmark, this.httpOptions).pipe(
      tap(_ => this.log(`updated bookmark id=${bookmark.id}`))
    );
  }

  addBookmark(bookmark: Bookmark): Observable<Bookmark> {
    return this.http.post<Bookmark>(this.bookmarksUrl, bookmark, this.httpOptions).pipe(
      tap((newBookmark: Bookmark) => this.log(`added bookmark w/ id=${newBookmark.id}`))
    );
  }

  deleteBookmark(bookmark: Bookmark | number): Observable<Bookmark> {
    const id = typeof bookmark === 'number' ? bookmark : bookmark.id;
    const url = `${this.bookmarksUrl}/${id}`;

    return this.http.delete<Bookmark>(url, this.httpOptions).pipe(
      tap(_ => this.log(`deleted bookmark id=${id}`))
    );
  }

  newBookmark(): Bookmark{
    return {
      id: null,
      label: null,
      url: null
    };
  }
}
