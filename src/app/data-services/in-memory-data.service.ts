import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Bookmark } from '../models/bookmark';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const bookmarks = [
      {id: 1, label: 'BBC news', url: 'https://www.bbc.co.uk/news'},
      {id: 2, label: 'Netflix', url: 'https://www.netflix.com'},
      {id: 3, label: 'YouTube', url: 'https://www.youtube.com'},
      {id: 4, label: 'Amazon', url: 'https://www.amazon.co.uk'},
    ];
    return {bookmarks};
  }

  genId(bookmarks: Bookmark[]): number {
    return bookmarks.length > 0 ? Math.max(...bookmarks.map(bookmark => bookmark.id)) + 1 : 11;
  }
}
